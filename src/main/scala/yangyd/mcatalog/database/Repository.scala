package yangyd.mcatalog.database

import javax.sql.DataSource

import org.springframework.stereotype.Component
import slick.driver.JdbcProfile
import slick.jdbc.JdbcBackend
import slick.util.AsyncExecutor

import scala.concurrent.{ExecutionContext, Future}

@Component
class Repository(profile: JdbcProfile,
                 dataSource: DataSource,
                 slickExecutor: AsyncExecutor) {
  import EntityDefinition._
  import EntitySupport._

  val entities = new EntityDefinition(profile)
  import entities._
  import entities.profile.api._

  val db = Database.forDataSource(dataSource, executor = slickExecutor)

  trait BaseRepository[T, A] extends DatabaseModule {
    override implicit val db: JdbcBackend#DatabaseDef = Repository.this.db
    override implicit val profile: JdbcProfile = Repository.this.profile
    protected val crud: GenericCRUD[T, A]
  }

  // Explicitly define repository and crud class so that Scala doesn't generate reflective calls.
  class CoffeeRepository extends BaseRepository[CoffeesTable, Coffee] {
    class CRUD extends CRUDSupport[CoffeesTable, Coffee](coffeeQ) {
      def listWithSupplier(): Future[Seq[(String, String)]] = {
        // Same as:
        // for { (c, s) <- coffeeQ join supplierQ on (_.supID === _.id) } yield (c.name, s.name)
        val innerJoin = for {
          c <- coffeeQ
          s <- supplierQ if c.supID === s.id
        } yield (c.name, s.name)

        db run innerJoin.result
      }
    }
    override val crud = new CRUD
  }
  val coffees = new CoffeeRepository

  val suppliers = new BaseRepository[SuppliersTable, Supplier] {
    override val crud = new CRUDSupport[SuppliersTable, Supplier](supplierQ) {}
  }

  def initDatabase()(implicit executionContext: ExecutionContext): Future[Unit] =
    db run schemas.drop recover { case e ⇒ } flatMap { _ ⇒ db run schemas.create }
}
