package yangyd.mcatalog.database

import org.springframework.context.annotation.{Bean, Configuration}
import slick.driver.JdbcProfile
import slick.util.AsyncExecutor

@Configuration
class SlickConfig {
  @Bean
  def slickExecutor(): AsyncExecutor = AsyncExecutor("slick", numThreads=10, queueSize=1000)

  @Bean
  def jdbcProfile(): JdbcProfile = {
    val mirror = scala.reflect.runtime.universe.runtimeMirror(classOf[slick.driver.JdbcProfile].getClassLoader)
    mirror.reflectModule(mirror.staticModule("slick.driver.HsqldbDriver")).instance.asInstanceOf[JdbcProfile]
  }
}
