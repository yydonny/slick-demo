package yangyd.mcatalog.database

import slick.driver.JdbcProfile
import yangyd.mcatalog.database.EntitySupport.BaseEntity

object EntityDefinition {
  case class Supplier(id: Long,
                      name: String,
                      street: String,
                      city: String,
                      state: String,
                      zip: String) extends BaseEntity

  case class Coffee(id: Long,
                    name: String,
                    supID: Long,
                    price: Double,
                    sales: Int,
                    total: Int) extends BaseEntity
}

class EntityDefinition(override val profile: JdbcProfile) extends EntitySupport(profile) {
  import EntityDefinition._
  import profile.api._

  class SuppliersTable(tag: Tag) extends BaseTable[Supplier](tag, "SUPPLIERS") {
    def name = column[String]("SUP_NAME")
    def street = column[String]("STREET")
    def city = column[String]("CITY")
    def state = column[String]("STATE")
    def zip = column[String]("ZIP")
    // Every table needs a * projection with the same type as the table's type parameter
    override def * = (id, name, street, city, state, zip) <> (Supplier.tupled, Supplier.unapply)
  }
  val supplierQ = TableQuery[SuppliersTable]

  class CoffeesTable(tag: Tag) extends BaseTable[Coffee](tag, "coffees") {
    def name = column[String]("COF_NAME")
    def supID = column[Long]("SUP_ID")
    def price = column[Double]("PRICE")
    def sales = column[Int]("SALES")
    def total = column[Int]("TOTAL")
    override def * = (id, name, supID, price, sales, total) <> ((Coffee.apply _).tupled, Coffee.unapply)

    def supplier = foreignKey("SUP_FK", supID, supplierQ)(
      _.id, onUpdate=ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Cascade) // foreign key consrtaint
  }
  val coffeeQ = TableQuery[CoffeesTable]

  override val schemas = coffeeQ.schema ++ supplierQ.schema

}
