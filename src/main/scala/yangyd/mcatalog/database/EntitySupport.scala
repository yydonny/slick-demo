package yangyd.mcatalog.database

import slick.driver.JdbcProfile
import slick.lifted.CanBeQueryCondition

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object EntitySupport {
  trait DatabaseProfile {
    val profile: JdbcProfile
  }

  trait DatabaseModule extends DatabaseProfile {
    val db: JdbcProfile#Backend#Database
  }

  trait BaseEntity {
    val id : Long
    def isValid : Boolean = true
  }
}

abstract class EntitySupport(val profile: JdbcProfile) { // Entity-specific profile needs to be exposed
  import EntitySupport._
  import profile.api._

  abstract class BaseTable[T](tag: Tag, name: String) extends Table[T](tag, name) {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  }

  val schemas: profile.SchemaDescription

  trait GenericCRUD[T,A] {
    def insert(row : A): Future[Long]
    def insert(rows : Seq[A]): Future[Seq[Long]]
    def update(row : A): Future[Int]
    def update(rows : Seq[A]): Future[Unit]
    def findById(id : Long): Future[Option[A]]
    def findByFilter[C : CanBeQueryCondition](f: (T) => C): Future[Seq[A]]
    def deleteById(id : Long): Future[Int]
    def deleteById(ids : Seq[Long]): Future[Int]
    def deleteByFilter[C : CanBeQueryCondition](f:  (T) => C): Future[Int]
  }

  class CRUDSupport[T <: BaseTable[A], A <: BaseEntity](tableQ: TableQuery[T])
                                                       (implicit val db: JdbcProfile#Backend#Database,
                                                        implicit val profile: JdbcProfile)
      extends GenericCRUD[T,A] with DatabaseProfile with DatabaseModule
  {
    override def insert(row: A): Future[Long] = insert(Seq(row)).map(_.head)

    override def insert(rows: Seq[A]): Future[Seq[Long]] =
      db.run(tableQ returning tableQ.map(_.id) ++= rows.filter(_.isValid))

    override def update(row: A): Future[Int] = {
      if (row.isValid)
        db.run(tableQ.filter(_.id === row.id).update(row))
      else
        Future { 0 }
    }

    override def update(rows: Seq[A]): Future[Unit] =
      db.run(DBIO.seq(rows.filter(_.isValid).map(r => tableQ.filter(_.id === r.id).update(r)): _*))

    override def findById(id: Long): Future[Option[A]] =
      db.run(tableQ.filter(_.id === id).result.headOption)

    override def findByFilter[C: CanBeQueryCondition](f: (T) => C): Future[Seq[A]] =
      db.run(tableQ.withFilter(f).result)

    override def deleteById(id: Long): Future[Int] = deleteById(Seq(id))

    override def deleteById(ids: Seq[Long]): Future[Int] = db.run(tableQ.filter(_.id.inSet(ids)).delete)

    override def deleteByFilter[C : CanBeQueryCondition](f:  (T) => C): Future[Int] = db.run(tableQ.withFilter(f).delete)
  }
}
