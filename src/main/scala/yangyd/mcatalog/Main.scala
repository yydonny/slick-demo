package yangyd.mcatalog

import org.springframework.boot.SpringApplication

object Main extends App {
  SpringApplication run classOf[MCatalog]
}
