package yangyd.mcatalog.util

import java.sql.SQLException

import org.springframework.beans.factory.annotation.{Autowired, Qualifier}
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import yangyd.mcatalog.database.EntityDefinition.{Coffee, Supplier}
import yangyd.mcatalog.database.Repository

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

@Component
@Profile(Array("development"))
class DataProvision @Autowired() (@Qualifier("cachedThreadPool") implicit val executionContext: ExecutionContext,
                                  repository: Repository) extends CommandLineRunner
{
  import repository._

  override def run(args: String*): Unit = {
    Future { Unit } flatMap { _ ⇒
      initDatabase()
    } flatMap { _ ⇒
      suppliers.crud.insert(Seq(
        Supplier(0, "Acme, Inc.",      "99 Market Street", "Groundsville", "CA", "95199"),
        Supplier(0, "Superior Coffee", "1 Party Place",    "Mendocino",    "CA", "95460"),
        Supplier(0, "The High Ground", "100 Coffee Lane",  "Meadows",      "CA", "93966")))

    } flatMap { supIDs: Seq[Long] ⇒
      val fk = () ⇒ supIDs(Random.nextInt(supIDs.length))
      coffees.crud.insert(Seq(
        Coffee(0, "Colombian",         fk(), 7.99, 0, 0),
        Coffee(0, "French_Roast",      fk(), 8.99, 0, 0),
        Coffee(0, "Espresso",          fk(), 9.99, 0, 0),
        Coffee(0, "Colombian_Decaf",   fk(), 8.99, 0, 0),
        Coffee(0, "French_Roast_Decaf",fk(), 9.99, 0, 0)))

    } recover {
      case e: SQLException ⇒ e.printStackTrace()

    } flatMap { _ ⇒
      coffees.crud.listWithSupplier()

    } flatMap { cs: Seq[(String, String)] ⇒
      cs foreach println
      coffees.crud.findByFilter(_ ⇒ true)

    } map (_.foreach {
      case Coffee(id, name, supID, price, sales, total) =>
        println(id + "  " + name + "\t" + supID + "\t" + price + "\t" + sales + "\t" + total)

    }) onFailure {
      case e ⇒ e.printStackTrace()
    }
  }
}
