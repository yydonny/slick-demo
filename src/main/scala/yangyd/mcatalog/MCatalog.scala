package yangyd.mcatalog

import java.util.concurrent.Executors

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean

import scala.concurrent.ExecutionContext

@SpringBootApplication
class MCatalog {
  @Bean(Array("cachedThreadPool"))
  def executionContext: ExecutionContext = ExecutionContext.fromExecutor(Executors.newCachedThreadPool())
}
