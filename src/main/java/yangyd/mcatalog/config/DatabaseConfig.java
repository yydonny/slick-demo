package yangyd.mcatalog.config;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

//@Configuration
//@ConfigurationProperties(prefix = "mcatalog.database")
public class DatabaseConfig {
  private static final Logger logger = LoggerFactory.getLogger(DatabaseConfig.class);
  private final Properties dsProps = new Properties();
  private final Properties hikariProps = new Properties();
//  @NotNull
  private String backend;
//  @NotNull
  private String concurrency;

  @Bean
  Config slickConfig() {
    checkBackend();

    int poolSize = Integer.parseInt(concurrency);
    Map<String, Object> root = new HashMap<>();
    Properties slickProps = new Properties();

    root.put("driver", slickDriver); // Slick Driver for the database backend
    root.put("db", slickProps);

    slickProps.put("numThread", poolSize);
    slickProps.put("connectionPool", "disabled"); // disable the pool managed by slick

    // http://slick.lightbend.com/doc/3.1.1/api/index.html#slick.jdbc.JdbcBackend$DatabaseFactoryDef@forConfig(String,Config,Driver,ClassLoader):Database
    slickProps.put("dataSourceClass", HikariDataSource.class.getName());
    slickProps.put("properties", hikariProps);

    // Managed Hikari configuration
    hikariProps.put("dataSourceClassName", dataSourceName);
    hikariProps.put("dataSourceProperties", dsProps);
    // http://slick.lightbend.com/doc/3.1.1/database.html#connection-pools
    // https://github.com/brettwooldridge/HikariCP/wiki/About-Pool-Sizing
    hikariProps.put("minimumIdle", poolSize);
    hikariProps.put("maximumPoolSize", poolSize);

    Config config = ConfigFactory.parseMap(root);
    logger.info("Effective Slick/Hikari Configuration: \n{}", config.root().render());

    return config;
  }

  private String slickDriver;
  private String dataSourceName;
  private void checkBackend() {
    switch (backend) {
      case "hsqldb":
        slickDriver = "slick.driver.HsqldbDriver$";
        dataSourceName = "org.hsqldb.jdbc.JDBCDataSource";
        logger.info("using database backend: HsqlDB");
        break;

      case "postgres":
        slickDriver = "slick.driver.PostgresDriver$";
        dataSourceName = "org.postgresql.ds.PGSimpleDataSource";
        logger.info("using database backend: PostgreSQL");
        break;
      default:
        logger.error("Unknown database backend. Choose a upported backend: [hsqldb|postgres]");
        throw new RuntimeException("Unsupported database backend: " + backend);
    }
  }

  /**
   * For Spring-boot configuration injection.
   */
  public void setConcurrency(String concurrency) {
    this.concurrency = concurrency;
  }

  /**
   * For Spring-boot configuration injection.
   */
  public void setBackend(String backend) {
    this.backend = backend;
  }

  /**
   * For Spring-boot configuration injection.
   */
  public Properties getDataSource() {
    return dsProps;
  }

  /**
   * For Spring-boot configuration injection.
   */
  public Properties getHikari() {
    return hikariProps;
  }
}
